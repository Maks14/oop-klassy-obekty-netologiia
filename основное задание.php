<?php
// ИНКАПСУЛЯЦИЯ - это создания однотипных свойств и методов для объектов, по общей логике, которые будут доступны этим объектам (присвоены), 
// после объявления их экземпляром класса
// ПЛЮСЫ И МИНУСЫ ОБЪЕКТОВ - Объекты обладают свойствами наследования, инкапсуляции и полиморфизма.
// Читаемость кода лучше. Меньше форс-мажоров. 
// Минусы незнаю) 
?>

<?php

class Car 
{   
    public $model;
    public $price;
    public $status;
   
    public function __construct ($model, $price, $status)
    {
     $this->model = $model;
     $this->price = $price;  
     $this->status = $status;
     
    //  if($this->status === 'Новая') {
    //   //  array_push($newCar, $this);
    //  } else if ($this->status === 'Поддержанная') {
    //   // array_push($oldCar, $this);
    //  }
    }

    public function getPrice () 
    {
     echo $this->price; 
    }

    public function getListNewCar () 
    {

    }

    public function getListOldCar () 
    {
      
    }
    
}

$audi = new Car('Ауди', '30000', 'Новая');
$bmw = new Car('БМВ', '100000', 'Поддержанная');

class TV
{
    public $model;
    public $price;
    public $color;
    public $diagonal;

    public function __construct ($model, $price, $color, $diagonal)
    {
     $this->model = $model;
     $this->price = $price;  
     $this->color = $color;
     $this->diagonal = $diagonal;
    }

}

$lg = new TV('LG', 150, 'black', 18);
$panasonic = new TV('Panasonic', 350, 'white', 30);


class BallPen
{
    public $price;
    public $color;
    public $kernel;
    

    public function __construct ($price, $color, $kernel)
    {
     $this->price = $price;  
     $this->color = $color;
     $this->kernel = $kernel;
    }

    public function getPrice () {
      
    }
}

$pen1 = new BallPen(250, 'синяя', 'толстый');
$pen2 = new BallPen(150, 'черная', 'тонкий');


class Duck 
{
    public $breed;
    public $price;
    public $color;
    public $kernel;
    

    public function __construct ($breed, $price, $color, $kernel)
    {
     $this->breed = $breed;   
     $this->price = $price;  
     $this->color = $color;
     $this->kernel = $kernel;
    }
}

$broiler = new Duck('Бройлер', 3, 'белая', 1);
$blagovarskaya =  new Duck('Благоварская', 100, 'белая', 2);

class Product 
{   
    public $title;
    public $description = [];
    public $price;

    public function __construct ($title, $price)
    {
     $this->title = $title;   
     $this->price = $price;  
    }
}

$product1 = new Product('Название товара', 'Цена товара');
$product2 = new Product('Название товара', 'Цена товара');

?>
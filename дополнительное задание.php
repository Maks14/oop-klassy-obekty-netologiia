class News 
{

    public $data = [];

    public function setNews($new) 
    {
      return array_push($this->data, $new);
    }

    public function getNewsNumber($number) 
    {
      return $this->data[$number-1];
    }

    public function getNewLast() 
    {
      return array_pop($this->data);
    }

    public function getNewsAll() 
    { 
      for ($i = 0; $i < count($this->data); $i++) {
        return $this->data[$i];
      }
    }

}

$news = new News();

$news->setNews('Первая новость');
$news->setNews('Вторая новость');
$news->setNews('Третья новость');
$news->setNews('Четвертая новость');

// echo $news->getNewsNumber(2);
// echo $news->getNewLast();
//echo $news->getNewsAll();

?>